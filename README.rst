.. -*- restructuredtext -*-

======================
*Dualité lagrangienne*
======================
Présentation pour l'examen oral de INFO-F524 *Continuous optimization* (ULB)

|



Author: 🌳 Olivier Pirson — OPi |OPi| 🇧🇪🇫🇷🇬🇧 🐧 👨‍💻 👨‍🔬
=================================================================
🌐 Website: http://www.opimedia.be/

💾 Bitbucket: https://bitbucket.org/OPiMedia/

* 📧 olivier.pirson.opi@gmail.com
* Mastodon: https://mamot.fr/@OPiMedia — Twitter: https://twitter.com/OPirson
* 👨‍💻 LinkedIn: https://www.linkedin.com/in/olivierpirson/ — CV: http://www.opimedia.be/CV/English.html
* other profiles: http://www.opimedia.be/about/

.. |OPi| image:: http://www.opimedia.be/_png/OPi.png

|



|Dualité lagrangienne|

(montage à partir de la `Gravure Joseph Louis de Lagrange`_ de Luigi Rados)

.. _`Gravure Joseph Louis de Lagrange`: http://www.larousse.fr/encyclopedie/images/Joseph_Louis_de_Lagrange/1004384

.. |Dualité lagrangienne| image:: https://bitbucket-assetroot.s3.amazonaws.com/c/photos/2020/Jun/13/2227990291-5-dualite-lagrangienne-logo_avatar.png
