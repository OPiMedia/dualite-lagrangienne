% -*- coding: utf-8 -*-
\documentclass[a4paper,ps2pdf,9pt]{beamer}
\usepackage{slides}

\title{Dualité lagrangienne}

\author{\href{http://www.opimedia.be/}{Olivier \textsc{Pirson}}}
\institute{Présentation pour l'examen oral de\\
  INFO-F524 \textit{Continuous optimization}\\[3ex]
  Département d'Informatique\\
  Université Libre de Bruxelles\\[1ex]
  \href{http://www.ulb.ac.be/}{\includegraphics[height=0.75cm]{img/ULB}}
}
\date{19 juin 2017\\[-1ex]
  {\tiny(Some corrections November 26, 2017)}\\
  \hspace*{-2em}%
  \href{https://bitbucket.org/OPiMedia/dualite-lagrangienne}{Dernière version :
    \path|{https://bitbucket.org/OPiMedia/dualite-lagrangienne}|}}

\hypersetup{pdfsubject={Présentation pour l'examen oral de INFO-F524 Continuous optimization (ULB).}}
\hypersetup{pdfkeywords={optimisation,relaxation,dual lagrangien}}

\logo{\includegraphics[width=1.55cm,height=1.15cm]{img/deux_Lagrange}}


\hyphenpenalty=10000
\sloppy

\begin{document}
\title{\includegraphics[width=8em]{img/deux_Lagrange}\\[2ex]
  {\Huge\textit{Dualité lagrangienne}}}
\begin{frame}[plain]
  \hypersetup{urlcolor=black}%
  \null\hspace*{-2.5em}%
  \parbox{\textwidth}{\titlepage}%
\end{frame}
\title{\textit{Dualité lagrangienne}}
\author{}
\institute{}
\date{}

\AtBeginSection{
  \begin{frame}
    \tableofcontents[currentsection]
  \end{frame}
}



\section{Introduction}
\begin{frame}
  \frametitle{Principe général de relaxation}
  Pour le programme\\
  $z = \max \{c(x) \mid x \in X \subseteq R^n\}$\\

  \bigskip
  \begin{block}{}
    $z^R = \max \{f(x) \mid x \in T \subseteq R^n\}$\\
    est une \textit{relaxation}
    si
    \begin{itemize}
    \item
      $X \subseteq T$
    \item
      $c(x) \leq f(x)\qquad\forall x \in X$
    \end{itemize}
    \begin{picture}(0,40)
      \put(190,0){\includegraphics[width=3cm]{img/relaxation}}
    \end{picture}
  \end{block}

  \bigskip
  $\Longrightarrow\ z^R$ est une borne supérieure du problème initial
\end{frame}


\begin{frame}
  \frametitle{Programme en nombre entiers ($PE$)}
  Soit\\
  $z = \max cx$\\
  sous les contraintes
  \begin{tabular}[t]{ll}
    $Ax \leq b$, & $k$ contraintes ``faciles''\\
    $Dx \leq d$, & $m$ contraintes ``difficiles''\\
    $x \in \naturals^n$
  \end{tabular}

  \bigskip
  avec
  \begin{tabular}[t]{l}
    $m, n \in \naturals*$\\
    $c \in \reals^n$\\
    $A \in \reals^{k,n}$\\
    $b \in \reals^k$\\
    $D \in \reals^{m,n}$\\
    $d \in \reals^m$
  \end{tabular}
\end{frame}


\begin{frame}
  \frametitle{Reformulation du problème}
  Remplaçons les contraintes ``faciles'' par une formulation implicite :\\
  $z = \max cx$\\
  s.c.
  \begin{tabular}[t]{ll}
    $Dx \leq d$, & $m$ contraintes ``difficiles''\\
    $x \in X = \{x \in \naturals^n \mid Ax \leq b\}$
  \end{tabular}
\end{frame}


\begin{frame}
  \frametitle{Relaxation lagrangienne ($RL(u)$)}
  \begin{block}{}
    $\forall u \in \reals_+^m$ on défini le problème $RL(u)$:\\
    \begin{tabular}{@{\quad}l}
      $z(u) = \max cx + \underbrace{u(d - Dx)}_{\text{pénalités}}$\\
      s.c. $x \in X$
    \end{tabular}

    \medskip
    $u$ est appelé multiplicateur de \textsc{Lagrange}
  \end{block}

  \bigskip
  $\forall u \in \reals_+^m : RL(u)$ est une relaxation de ($PE$) car\\[1ex]
  \begin{itemize}
  \item
    $\{x \in X \mid Dx \leq d\} \subseteq X$

  \item
    $\begin{array}{@{}l|}
    u \geq 0\\
    Dx \leq d
  \end{array}
    \Longrightarrow\quad
    cx + u(d - Dx) \geq cx$
  \end{itemize}

  \bigskip
  $\Longrightarrow\ RL(u)$ fournit une borne supérieure pour ($PE$)
\end{frame}



\section{Dual lagrangien}
\begin{frame}[t]
  \frametitle{Dual lagrangien}
  $z(u) = \max cx + u(d - Dx)$

  \medskip
  Prenons la meilleure relaxation possible :\\
  $w_{LD} = \min\limits_{u\geq 0} z(u)$

  \only<2>{\addtocounter{framenumber}{1}%
    \begin{block}{\textbf{Proposition}}
      Une solution $x(u)$ optimale de $RL(u)$\\
      qui est admissible pour ($EP$)\\
      et telle que $(Dx(u))_i = d_i\text{ si }u_i > 0$ (complémentarité)
      \footnote{
        Toujours vrai si contraintes d'égalités.}\\
      est aussi optimale pour ($EP$)
    \end{block}

    En effet :\\
    $w_{LD} = \min\limits_{u\geq 0} z(u) \leq cx(u) + u(d - Dx(u)) = cx(u)$

    \medskip
    $\left.\begin{array}{@{}l}
      x(u) \in X\\
      Dx(u) \leq d
    \end{array}\right\}
    \Longrightarrow\ cx(u) \leq z^{\star}$

    \medskip
    $z^{\star} \leq w_{LD}\ \Longrightarrow\ x(u)$ est solution optimale de ($PE$)}
\end{frame}



\section{Qualité du dual lagrangien}
\begin{frame}
  \frametitle{Qualité du dual lagrangien}
  Supposons, par simplicité, $X = \{x^1, x^2, \dots, x^T\}$ fini, avec $T$ très grand

  \medskip
  $w_{LD}
  \begin{array}[t]{l}
    = \min\limits_{u\geq 0} z(u)\\
    = \min\limits_{u\geq 0} \{\max\limits_{x \in X} cx + u(d - Dx)\}\\
    = \min\limits_{u\geq 0} \{\max\limits_{t=1,\dots,T} cx^t + u(d - Dx^t)\}\\
    =
    \begin{array}[t]{@{\ }l}
      \min \eta\\
      \text{s.c.}
      \begin{array}[t]{@{\ }l}
        \eta \geq cx^t + u(d - Dx^t)\text{ pour }t = 1,\dots,T\\
        u \in \reals_+^m\\
        \eta \in \reals
      \end{array}
    \end{array}
  \end{array}$

  \medskip
  $\eta$ représente une borne supérieure de $z(u)$
\end{frame}


\begin{frame}
  \frametitle{Qualité du dual lagrangien}
  \begin{block}{}
    $w_{LD}
    =
    \begin{array}[t]{@{\ }l}
      \min \eta\\
      \text{s.c.}
      \begin{array}[t]{@{\ }l}
        \eta \geq cx^t + u(d - Dx^t)\text{ pour }t = 1,\dots,T\\
        u \in \reals_+^m\\
        \eta \in \reals
      \end{array}
    \end{array}$\\
    est un programme linéaire.
  \end{block}

  \begin{block}{}
    Prenons son dual :\\
    $w_{LD}
    =
    \begin{array}[t]{@{\ }l}
      \max \sum\limits_{t=1}^T \mu_t cx^t\\
      \text{s.c.}
      \begin{array}[t]{@{\ }l}
        \sum\limits_{t=1}^T \mu_t = 1\\
        - \sum\limits_{t=1}^T \mu_t (d - Dx^t) \leq 0\\
        \mu_t \in \reals_+^T
      \end{array}
    \end{array}$
  \end{block}
\end{frame}

\begin{frame}
  \frametitle{Qualité du dual lagrangien}
  $w_{LD}
  =
  \begin{array}[t]{@{\ }l}
    \max \sum\limits_{t=1}^T \mu_t cx^t\\
    \text{s.c.}
    \begin{array}[t]{@{\ }l}
      \sum\limits_{t=1}^T \mu_t = 1\\
      - \sum\limits_{t=1}^T \mu_t (d - Dx^t) \leq 0\\
      \mu_t \in \reals_+^T
    \end{array}
  \end{array}$

  \begin{block}{\textbf{Théorème}}
    En posant $x = \sum\limits_{t=1}^T \mu_t x^t$
    avec $\sum\limits_{t=1}^T \mu_t = 1$
    et $\mu_t \in \reals_+^T$ :\\
    $w_{LD}
    =
    \begin{array}[t]{@{\ }l}
      \max cx\\
      \text{s.c.}
      \begin{array}[t]{@{\ }l}
        Dx \leq d\\
        x \in conv(X)
      \end{array}
    \end{array}$
  \end{block}
  Il est possible de montrer que ce résultat reste valable pour $X$ infini.
\end{frame}


\begin{frame}
  \frametitle{Qualité du dual lagrangien}
  La preuve de ce résultat révèle la structure du dual lagrangien.

  $z(u)$ est convexe, linéaire par morceaux
  et non différentiable.

  \begin{figure}
    \includegraphics[width=16em]{img/forme_du_probleme_dual}\\
    \caption{[Wolsey 1998]}
  \end{figure}
\end{frame}


\begin{frame}
  \frametitle{Qualité du dual lagrangien}
  \begin{block}{\textbf{Corollaire}}
    Si $conv(X) = \{x \in \reals_+^n \mid Ax \leq b\}$\\
    alors
    $w_{LD} = \max\{cx \mid Ax \leq b, Dx \leq d, x \in \reals_+^n\}
    = z_{\text{relaxation linéaire}}$
  \end{block}

  \bigskip
  Moyen de résoudre un programme linéaire\\
  avec un nombre exponentiel de contraintes\\
  sans les traiter explicitement.
\end{frame}



\section{Résolution du dual lagrangien}
\begin{frame}
  \frametitle{Résolution du dual lagrangien}
  \begin{block}{}
    $w_{LD} = \min\limits_{u\geq 0} z(u)$\\
    avec
    $z(u) = \max\limits_{t=1,\dots,T} \{cx^t + u(d - Dx^t)\}$
  \end{block}

  Grand nombre de contraintes.

  \medskip
  Algorithme de plans coupants souvent difficile et peu efficace.
\end{frame}


\begin{frame}
  \frametitle{Sous-gradient}
  \begin{block}{\textbf{Définition}}
    Soit $f : \reals^m \longrightarrow \reals$ convexe.\\
    Un \textit{sous-gradient} en $u$ de $f$ est un vecteur $\gamma(u) \in \reals^m$ tel que\\
    $f(v) \geq f(u) + \gamma(u)^T (v - u)\qquad\forall v \in \reals^m$
  \end{block}

  \medskip
  Si $f$ est de classe $C^1$ alors c'est le gradient\\
  $\gamma(u) = \nabla f(u)
  = \left(\frac{\partial f}{\partial u_1}, \frac{\partial f}{\partial u_2}, \dots,
  \frac{\partial f}{\partial u_m},\right)(u)$
\end{frame}


\begin{frame}
  \frametitle{Algorithme de sous-gradient}
  \begin{block}{}
    \textbf{Initialisation :}
    $u = u^0$

    \textbf{Itération $k$ : $u = u^k$}
    \begin{itemize}
    \item
      Résoudre $RL(u^k)$, de solution optimale $x(u^k)$
    \item
      $u^{k+1} = \max\{0, u^k - \mu_k(d - Dx(u^k))\}$
    \item
      $k \leftarrow k + 1$
    \end{itemize}
  \end{block}

  \medskip
  $d - Dx(u^k)$ est un sous-gradient de $z(u)$ en $u^k$

  \bigskip
  Algorithme simple, mais difficulté de choisir les pas $\mu_k$
\end{frame}


\begin{frame}
  \frametitle{Algorithme de sous-gradient}
  \begin{block}{\textbf{Théorème}}
    \begin{itemize}
    \item
      $\begin{array}{@{}l|}
      \sum\limits_k \mu_k \rightarrow \infty\\
      \mu_k \rightarrow 0
    \end{array}
      \Rightarrow z(\mu_k) \rightarrow w_{LD}$

      \bigskip
    \item
      $\begin{array}{@{}l|}
      \rho < 1\\
      \mu_k = \mu_0 \rho^k\\
      \mu_0\text{ et }\rho\text{ suffisamment grands}
    \end{array}
      \Rightarrow z(\mu_k) \rightarrow w_{LD}$

      \bigskip
    \item
      $\begin{array}{@{}l|}
      \overline{w} \geq w_{LD}\\
      \mu_k = \varepsilon_k \frac{z(u^k) - \overline{w}}{\|d - Dx(u^k)\|^2}\\
      0 < \varepsilon_k < 2
    \end{array}
      \Rightarrow
      \begin{array}{|l@{}}
        z(\mu_k) \rightarrow w_{LD}\\
        \text{ou l'algorithme trouve $u^k$ tel que}\\
        \qquad\overline{w} \geq z(\mu_k) \geq w_{LD}
      \end{array}$
    \end{itemize}
  \end{block}
\end{frame}



\section{Références}
\begin{frame}
  \frametitle{Références}
  Références :
  \small
  \begin{itemize}
  \item\label{INFO-F524}
    Bernard \textsc{Fortz}.\\
    Cours \textbf{INFO-F524 \textit{Continuous optimization}}.\\
    Université Libre de Bruxelles, 2017\\

  \item\label{Wolsey 1998}
    Laurence A. \textsc{Wolsey}.\\
    \textbf{\textit{Integer Programming}}.\\
    Wiley, 1998

    \bigskip
  \item\label{Lagrange}
    Luigi \textsc{Rados} d'après \textsc{Bosio}.\\
    \href{http://www.larousse.fr/encyclopedie/images/Joseph_Louis_de_Lagrange/1004384}{Gravure \textit{Joseph Louis \textsc{de Lagrange}}}.\\
    Académie des Sciences, Paris
  \end{itemize}
\end{frame}

\end{document}
